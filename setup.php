<?php

// setup.php
define('SITE_URL', 'http://127.0.0.1');
define('ROOT', realpath(__DIR__));
define('CONTROLLER', ROOT . '/controller');
define('ENTITY', ROOT . '/entity');
define('VIEW', ROOT . '/view');
define('DAO', ROOT . '/dao');
define('CONFIG', ROOT . '/config');
define('INI_FILE', CONFIG . '/conf.ini');
define('UPLOADS', ROOT . '\uploads');

define('ERR_AUTH_PWD', "Erreur de mot de passe.");
define('ERR_AUTH_BLOCKED', "Votre compte est bloqué, veuillez contacter l'administrateur");
define('ERR_AUTH_LAST_ATTEMPT', "Erreur de mot de passe. Il vous reste une tentative !");
define('MAX_ATTEMPT_AUTH', 3);

