<?php

class FctUtil {

    public static function reName($fichier, $id) {
        $infosfichier = pathinfo($fichier['name']);
        $extension_upload = $infosfichier['extension'];
        $today = date("Ym");
        $dao = new MyDao();
        $resu = $dao->getFHFMaxIdLFHF();
        $idLFHF = $resu[0]['MAX'] + 1;
        return $nom = $today . $id . $idLFHF . "." . $extension_upload;
    }
    
    public static function updateName($fichier, $id, $idLFHF) {
        $infosfichier = pathinfo($fichier['name']);
        $extension_upload = $infosfichier['extension'];
        $today = date("Ym");
        return $nom = $today . $id . $idLFHF . "." . $extension_upload;
    }

    public static function recordFile($fichier, $name) {
//        var_dump($fichier);
        $chemin = UPLOADS . "/" . $name;
//        var_dump($chemin);
        //copie le fichier de son emplacement temporaire à l'emplacement final
        $result = move_uploaded_file($fichier['tmp_name'], $chemin);
        return $result;
    }

    public static function deleteFile($idLFHF, $id) {
        $today = date("Ym");
        //donne le chemin du fichier avec l'année mois, id Utilisateur et id LFHF
        // et à la fin le ".*" pour trouver l'extension
        $nom = UPLOADS . "/" . $today . $id . $idLFHF . ".*";
        //glob permet de chercher un fichier dans un répertoire en sachant
        // qu'une partie du nom du fichier
        $chemin = glob($nom);
//        var_dump($chemin);
//        die();
        //supprime le fichier
        $result = unlink($chemin[0]);
        return $result;
    }

}
