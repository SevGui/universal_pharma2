"use strict";

document.addEventListener("DOMContentLoaded", init, false);

function init() {
    var myForm = document.getElementById("myForm");
    var errorMsgMontant = document.getElementById("errorMsgMontant");
    var errorMsgDate = document.getElementById("errorMsgDate");
    
    myForm.addEventListener("submit", function (event) {
        //arrête l'envoie du POST
        event.preventDefault();
        // condition: chiffre de 0 à 9 d'une longueur de 1 à 4
        // myForm.qt.value récupère la valeur de quantité dans le html
        if (/^[0-9]{1,5}.?[0-9]{0,2}?$/.test(myForm.montant.value)) { 

            if (/^20[0-9]{2}-[0-9]{2}-[0-9]{2}$/.test(myForm.datej.value)){
                // Renvoie le POST si le test est concluant
                myForm.submit();
            } else {
                //sinon renvoi un message d'erreur
                errorMsgDate.innerHTML = "Entrez une date au format aaaa-mm-jj";
            }
            
        } else {
            //sinon renvoi un message d'erreur
            errorMsgMontant.innerHTML = "Entrez un prix inférieur à 10000 euros";
        }

    }, false);
}


