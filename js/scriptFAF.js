"use strict";

document.addEventListener("DOMContentLoaded", init, false);

function init() {
    var myForm = document.getElementById("myForm");
    var errorMsg = document.getElementById("errorMsg");
    
    myForm.addEventListener("submit", function (event) {
        //arrête l'envoie du POST
        event.preventDefault();
        // condition: chiffre de 0 à 9 d'une longueur de 1 à 4
        // myForm.qt.value récupère la valeur de quantité dans le html
        if (/^[0-9]{1,4}$/.test(myForm.qt.value)) { 
            // Renvoie le POST si le test est concluant
            myForm.submit();
        } else {
            //sinon renvoi un message d'erreur
            errorMsg.innerHTML = "Entrez un nombre entier de 4 chiffres maximum";
        }

    }, false);
}


