"use strict";

function consult(datec) {

    var errorMsg = document.getElementById("errorMsgDateC");

    if (/^20[0-9]{2}[0-9]{2}$/.test(datec)) {
        get_frais_ajax_request(datec);
    } else {
        //sinon renvoi un message d'erreur
        errorMsg.innerHTML = "Entrez une date au format aaaamm";
    }

}

function get_frais_ajax_request(datec) {
    var content = document.getElementById("content");
    var xhr = new XMLHttpRequest();

    xhr.open("GET", "http://127.0.0.1/Universal-Pharma/controller/c_consult.php?anneemois=" + datec, true);
//    alert("yo");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var frais = xhr.responseText;
            content.innerHTML = frais;
        }
    };
    xhr.send(null);
}


