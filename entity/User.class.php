<?php

/**
 * Description of user
 * Correspond aux colonnes de la table Utilisateur
 * @author sev
 */
class user {

    //Attributs
    private $id;
    private $nom;
    private $prenom;
    private $login;
    private $mdp;
    private $adresse;
    private $cp;
    private $ville;
    private $datembauche;
    private $civil;
    private $mail;
    private $etat;
    private $idrole;
    private $role;
    private $logAttempt;

    //Méthodes

    function __construct($id, $nom, $prenom, $login, $mdp, $adresse, $cp, $ville, $datembauche, $civil, $mail, $etat, $idrole, $role, $logAttempt) {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->login = $login;
        $this->mdp = $mdp;
        $this->adresse = $adresse;
        $this->cp = $cp;
        $this->ville = $ville;
        $this->datembauche = $datembauche;
        $this->civil = $civil;
        $this->mail = $mail;
        $this->etat = $etat;
        $this->idrole = $idrole;
        $this->role = $role;
        $this->logAttempt = $logAttempt;
    }

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getPrenom() {
        return $this->prenom;
    }

    public function getLogin() {
        return $this->login;
    }

    public function getMdp() {
        return $this->mdp;
    }

    public function getAdresse() {
        return $this->adresse;
    }

    public function getCp() {
        return $this->cp;
    }

    public function getVille() {
        return $this->ville;
    }

    public function getDatembauche() {
        return $this->datembauche;
    }

    public function getCivil() {
        return $this->civil;
    }

    public function getMail() {
        return $this->mail;
    }

    public function getEtat() {
        return $this->etat;
    }

    public function getIdrole() {
        return $this->idrole;
    }

    public function getRole() {
        return $this->role;
    }

    public function getLogAttempt() {
        return $this->logAttempt;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    public function setLogin($login) {
        $this->login = $login;
    }

    public function setMdp($mdp) {
        $this->mdp = $mdp;
    }

    public function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    public function setCp($cp) {
        $this->cp = $cp;
    }

    public function setVille($ville) {
        $this->ville = $ville;
    }

    public function setDatembauche($datembauche) {
        $this->datembauche = $datembauche;
    }

    public function setCivil($civil) {
        $this->civil = $civil;
    }

    public function setMail($mail) {
        $this->mail = $mail;
    }

    public function setEtat($etat) {
        $this->etat = $etat;
    }

    public function setIdrole($idrole) {
        $this->idrole = $idrole;
    }

    public function setRole($role) {
        $this->role = $role;
    }

    public function setLogAttempt($logAttempt) {
        $this->logAttempt = $logAttempt;
    }

    
    /*
     * function checkAutentication
     * TEST de vérification de l'autentication
     * et si il y a une erreur 
     * on retourne un tab asso avec une clé error et la raison en valeur
     */

    function checkAutentication($password) {
        $response = [];
        $dao = new MyDao();
        //si l'utilisateur n'est pas bloqué je continue la validation
        if ($this->isBlocked()) {
            $response['error'] = ERR_AUTH_BLOCKED;
        } elseif ($this->getMdp() !== $password) {
            $dao->incrementLogAttempt($this);
            $response['error'] = ERR_AUTH_PWD;
            if ($this->getLogAttempt() === MAX_ATTEMPT_AUTH - 1) {
                $response['error'] = ERR_AUTH_LAST_ATTEMPT;
            } elseif ($this->getLogAttempt() === MAX_ATTEMPT_AUTH) {
                $response['error'] = ERR_AUTH_BLOCKED;
            }
        } else {
            $response['success'] = true;
        }
        //sinon j'assigne une erreur indiquant que son compte est bloqué
        return $response;
    }

    /*
     * function isBlocked
     * Bloque le password et renvoie un msg d'erreur
     */

    public function isBlocked() {
        //vérifier que la propriété logAttempt >=3 si oui on retourne true si non false
        return $this->getLogAttempt() >= MAX_ATTEMPT_AUTH;
    }

}
