<?php

/**
 * Description of FAF
 * Correspond aux colonnes de la table Ligne Frais Au Forfait (Ligne FAF)
 * @author sev
 */
class FAF {

    //Attributs
    private $idUtilisateur;
    private $anneeMois;
    private $idFAF;
    private $quantite;
    private $idLFAF;

    //Méthodes

    public function __construct($idUtilisateur, $anneeMois, $idFAF, $quantite, $idLFAF = NULL) {
        $this->idUtilisateur = $idUtilisateur;
        $this->anneeMois = $anneeMois;
        $this->idFAF = $idFAF;
        $this->quantite = $quantite;
        $this->idLFAF = $idLFAF;
    }

    public function getIdUtilisateur() {
        return $this->idUtilisateur;
    }

    public function getAnneeMois() {
        return $this->anneeMois;
    }

    public function getIdFAF() {
        return $this->idFAF;
    }

    public function getQuantite() {
        return $this->quantite;
    }

    public function getIdLFAF() {
        return $this->idLFAF;
    }

    public function setIdUtilisateur($idUtilisateur) {
        $this->idUtilisateur = $idUtilisateur;
    }

    public function setAnneeMois($anneeMois) {
        $this->anneeMois = $anneeMois;
    }

    public function setIdFAF($idFAF) {
        $this->idFAF = $idFAF;
    }

    public function setQuantite($quantite) {
        $this->quantite = $quantite;
    }

    public function setIdLFAF($idLFAF) {
        $this->idLFAF = $idLFAF;
    }

}
