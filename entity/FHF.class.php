<?php

/**
 * Description of FHF
 * Correspond aux colonnes de la table Ligne Frais Hors Forfait (Ligne FHF)
 * @author sev
 */
class FHF {

    //Attributs
    private $idLFHF;
    private $idUtilisateur;
    private $anneeMois;
    private $libelle;
    private $dateJustif;
    private $montant;
    private $scan;

    //Méthodes

    public function __construct($idUtilisateur, $anneeMois, $libelle, $montant, $dateJustif = NULL, $scan = NULL, $idLFHF= NULL) {
        $this->idLFHF = $idLFHF;
        $this->idUtilisateur = $idUtilisateur;
        $this->anneeMois = $anneeMois;
        $this->libelle = $libelle;
        $this->dateJustif = $dateJustif;
        $this->montant = $montant;
        $this->scan = $scan;
    }

    public function getIdLFHF() {
        return $this->idLFHF;
    }

    public function getIdUtilisateur() {
        return $this->idUtilisateur;
    }

    public function getAnneeMois() {
        return $this->anneeMois;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function getDateJustif() {
        return $this->dateJustif;
    }

    public function getMontant() {
        return $this->montant;
    }

    public function getScan() {
        return $this->scan;
    }

    public function setIdLFHF($idLFHF) {
        $this->idLFHF = $idLFHF;
    }

    public function setIdUtilisateur($idUtilisateur) {
        $this->idUtilisateur = $idUtilisateur;
    }

    public function setAnneeMois($anneeMois) {
        $this->anneeMois = $anneeMois;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    public function setDateJustif($dateJustif) {
        $this->dateJustif = $dateJustif;
    }

    public function setMontant($montant) {
        $this->montant = $montant;
    }

    public function setScan($scan) {
        $this->scan = $scan;
    }



}
