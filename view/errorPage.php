<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Universal Pharma</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/CSS" href="../third_party/bootstrap-3.3.6-dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/csshomepage.css">
        <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    </head>
    <body>
        <div class="row col-lg-10 col-lg-offset-1">
            <!-- DEBUT : Barre de navigation -->
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-brand">
                        <img id='uplogo' src='../images/universalpharmalogo.jpg' alt='uplogo'/>
                    </div> 

                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a href="../view/v_tabbord.php">Tableau de bord</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><form class="navbar-form navbar-right" method="post" action="../controller/c_homepage.php">  
                                    <button type="submit" class="btn btn-default">Déconnexion</button>
                                </form></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- FIN : Barre de navigation -->

            <!--DEBUT SOUS HEADER-->  
            <div class="col-md-12">    
                <div class="row" id="encart_vert">
                    <div class="col-sm-1">
                        <!--<img id="picot-ss-head" src="../images/TBBORD.png">-->
                    </div>
                    <div class="col-sm-2">
                        <h5 id="sous_head">Bonjour <?php echo $nom ?></h5>                 
                    </div>
                    <div class="col-sm-9 text-right">
                        <h5 id="sous_head">Rôle : <?php echo $role ?></h5>
                    </div>
                </div>
            </div> 
            <!--FIN SOUS HEADER-->  

            <!--DEBUT BLOC TEXTE--> 
            <div class="col-md-12 text-center"  >
                <br>
                <h2 id="titre_block-texte">Oups...</h2>
                <p id="block-texte">
                <div><?php echo $msg ?></div>             
                </p>
                <br>
            </div>
            <!--FIN BLOC TEXTE--> 

            <div id="blanc" class="row"> 
                <div class="col-md-12"></div>               
            </div>

            <!-- DEBUT : LOGO -->
            <div class="container logo" id="logo_page">    
                <div class="row">
                    <div class="col-sm-4 col-sm-push-4">
                        <img src="../images/universalpharmalogo.jpg" class="img-responsive" alt="Image">
                        <br>
                    </div>
                </div>
            </div>
            <!-- FIN : LOGO -->

            <!-- DEBUT : FOOTER -->
            <footer class="container-fluid" id="encart_footer">

                <div class="row" id="footer_MotionLegale">
                    <div class="col-xs-12">
                    </div>
                    <p id="footer_texte_MentionLegale">Universal Pharma : 72, Quai des carrières Vitry/Seine. &copy 2008 UNIVERSAL PHARMA Tous droits réservés.</p>
                </div>
            </footer>
            <!-- FIN : FOOTER -->
        </div>
    </body>
</html>