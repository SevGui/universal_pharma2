<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Universal Pharma</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/CSS" href="third_party/bootstrap-3.3.6-dist/css/bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/csshomepage.css">
        <link rel="icon" type="image/x-icon" href="./images/favicon.ico" />
    </head>
    <body class="row col-lg-10 col-lg-offset-1">

        <!-- DEBUT : Barre de navigation -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-brand">
                    <img id='uplogo' src='images/universalpharmalogo.jpg' alt='uplogo'/>
                </div> 
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="nav navbar-nav"><a href="index.php">Accueil</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <!-- DEBUT : FORMULAIRE-->
                        <li><form class="navbar-form navbar-right" role="search" method="post" action="./controller/c_homepage.php">
                                <div class="form-group">
                                    <label id="textebuton" for="login">Login</label>
                                    <input type="text" class="form-control" name="login" placeholder="Login">
                                </div>
                                <div class="form-group">
                                    <label id="textebuton" for="Motdepasse">Mot de passe</label>
                                    <input type="password" class="form-control" name="password" placeholder="Mot de passe">
                                </div>
                                <button type="submit" class="btn btn-default">Valider</button>
                            </form></li>
                        <!-- FIN : FORMULAIRE-->
                    </ul>
                </div>
            </div>
        </nav>
        <!-- FIN : Barre de navigation -->

        <!-- DEBUT : VIDEO de PRESENTATION -->
        <div class="embed-responsive embed-responsive-16by9">
            <video autoplay loop  controls="" class="embed-responsive-item">
                <source src="images/VIDEO_CCP01_2.mp4" type="video/mp4">
            </video>     
        </div>
        <!-- FIN : VIDEO de PRESENTATION -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : LOGO -->
        <div class="container logo" id="logo_page">    
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <img src="images/universalpharmalogo.jpg" class="img-responsive" alt="Image">
                </div>
            </div>
        </div>
        <!-- FIN : LOGO -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : ACCROCHE n°1 -->
        <div class="container-fluid text-center">    
            <div class="row" id="encart_vert">
                <div class="col-sm-4 col-sm-push-4">
                    <br>
                    <h3>Universal Pharma <br>leader depuis 2012 du secteur industriel Pharmaceutique.<br></h3>
                    <br>
                    <br>
                </div>
            </div>
        </div>   
        <!-- FIN : ACCROCHE n°1 -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : LOGO -->
        <div class="container logo" id="logo_page">    
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <img src="images/universalpharmalogo.jpg" class="img-responsive" alt="Image">
                </div>
            </div>
        </div>
        <!-- FIN : LOGO -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>
        <!-- DEBUT : BLOC TEXTE Qui Sommes Nous ?-->
        <div id="TITRE_Bloc_text_story" class="container-fluid text-center"> 
            <div class="col-md-12">
                <p>Qui Sommes Nous ?</p>
            </div> 
        </div>

        <div class="col-sm-6 col-xs-12">
            <p id="bloc_text_story">
                Universal Pharma comprend plusieurs laboratoires de recherche à Vitry/Seine et dix établissements qui fabriquent des médicaments.<br>
                <br>
                Universal Pharma est née de la fusion de pkusieurs firmes américaines (spécialisée dans le secteur des maladies virales dont le SIDA et les hépatites) et européennes.<br>
            </p>
        </div>  

        <div class="col-sm-6 col-xs-12">
            <p id="bloc_text_story">
                En 2012, les deux géants pharmaceutiques ont uni leurs forces pour créer un leader de ce secteur industriel.<br>
                <br>
                L'entité Universal Pharma Europe a établi son siège administratif à Paris.<br>
                <br>
                La France a été choisie comme témoin pour l'amélioration du suivi.
            </p>
        </div>
        <!-- FIN : BLOC TEXTE Qui Sommes Nous ?-->

        <div id="BigBlanc" class="row"> 
            <div class="col-xl-12"></div>               
        </div>    

        <!-- DEBUT : CAROUSSEL-->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="images/photo_carousel_01.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>

                <div class="item">
                    <img src="images/photo_carousel_02.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>

                <div class="item">
                    <img src="images/photo_carousel_03.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>

                <div class="item">
                    <img src="images/photo_carousel_04.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>

                <div class="item">
                    <img src="images/photo_carousel_05.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>
            </div>           

            <!--Left and right controls-->
            <!-- les flèches sont-elles utiles ???
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>-->
        </div>
        <!-- FIN : CAROUSSEL-->

        <div id="BigBlanc" class="row"> 
            <div class="col-xl-12"></div>               
        </div>

        <!-- DEBUT : PICTO UP-->
        <div class="row text-center" id="texte_picto">    
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <p><b>8 000</b><br>comptes mails</p>
                    <br>
                    <br>
                    <img id="picto" src="images/email.png" class="img-responsive" alt="Image">
                </div>
                <div class="col-sm-3 col-xs-3">
                    <p><b>1</b> dizaines<br> d'applications<br> de gestion</p>  
                    <br>
                    <img id="picto" src="images/smartphone-call.png" class="img-responsive" alt="Image">
                </div>
                <div class="col-sm-3 col-xs-3">
                    <p><b>100 To</b> <br>de données <br>sauvegardées</p> 
                    <br>
                    <img id="picto" src="images/interface.png" class="img-responsive" alt="Image">
                </div>
                <div class="col-sm-3 col-xs-3">
                    <p><b>3000</b><br> postes<br> informatiques</p>
                    <br>
                    <img id="picto" src="images/computerlogo.png" class="img-responsive" alt="Image">     
                </div>
            </div>
        </div>
        <!-- FIN : PICTO UP-->

        <div id="BigBlanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : ACCROCHE n°2 -->
        <div class="container-fluid text-center">    
            <div class="row" id="encart_vert">
                <div class="col-sm-4 col-sm-push-4">
                    <br>
                    <h3>Universal Pharma <br>c'est 1200 consultants médicaux.<br></h3>
                    <br>
                    <br>
                </div>
            </div>
        </div>
        <!-- FIN : ACCROCHE n°2 -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : LOGO -->
        <div class="container logo" id="logo_page">    
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <img src="images/universalpharmalogo.jpg" class="img-responsive" alt="Image">
                </div>
            </div>
        </div>
        <!-- FIN : LOGO -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : FOOTER -->
        <footer class="container-fluid" id="encart_footer">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">NOUS CONTACTER</h5>                  
                    <a href="#" id="footer_text">Contactez-nous par e-mail</a> 
                    <br>
                    <a href="#" id="footer_text">Contactez-nous par téléphone : +33 (0) 170489073</a>  
                    <br>
                    <a href="https://www.google.fr/maps/place/72+Quai+des+Carri%C3%A8res,+94220+Charenton-le-Pont/@48.8202625,2.4029738,17z/data=!3m1!4b1!4m5!3m4!1s0x47e672569eebee53:0x3b9f7e9b714d4683!8m2!3d48.820259!4d2.4051625" id="footer_text">Contactez-nous par courrier postal</a>                    
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">LIENS</h5>
                    <a  href="http://www.medicaments.social-sante.gouv.fr/" id="footer_text">médicaments.gouv.fr</a>
                    <br>
                    <a href="https://www.vidal.fr/" id="footer_text">vidal.fr</a>
                    <br>
                    <a href="http://www.ameli.fr/" id="footer_text">ameli.fr</a>
                    <br>
                    <a href="https://www.data.gouv.fr/fr/" id="footer_text">data.gouv.fr</a>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">RÉSEAUX SOCIAUX</h5>
                    <div id="picto_reso" class="col-sm-3"><a href='#'><img class="youtube"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="https://www.facebook.com/profile.php?id=100012325773575"><img class="fb"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="#"><img class="twitter"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href='#'><img class="linkedin"/></a></div>
                </div>
            </div>  
            <br>
            <div class="row" id="footer_MotionLegale">
                <div class="col-xs-12">
                </div>
                <p id="footer_texte_MentionLegale">Universal Pharma : 72, Quai des carrières Vitry/Seine. &copy 2008 UNIVERSAL PHARMA Tous droits réservés.</p>
            </div>
        </footer>
        <!-- FIN : FOOTER -->
    </body>
</html>
