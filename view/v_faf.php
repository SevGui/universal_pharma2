<?php
session_start();
$nom = null;
$prenom = null;
$role = null;
$id = NULL;
if (isset($_SESSION['nom'])) {
    $nom = $_SESSION['nom'];
    $prenom = $_SESSION['prenom'];
    $role = $_SESSION['role'];
    $id = $_SESSION['id'];
}

include '../setup.php';
include'../dao/MyDao.php';

$dao = new MyDao();
$data = $dao->getFAFByIdAnneeMois($id);
//var_dump($data);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Universal Pharma</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/CSS" href="../third_party/bootstrap-3.3.6-dist/css/bootstrap.css">
        <script src="../js/scriptFAF.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/csshomepage.css">
        <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    </head>
    <body class="row col-lg-10 col-lg-offset-1">
        <!-- DEBUT : Barre de navigation -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-brand">
                    <img id='uplogo' src='../images/universalpharmalogo.jpg' alt='uplogo'/>
                </div> 
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="v_tabbord.php">Tableau de bord</a></li>
                        <li><a href="v_cptuser.php">Compte</a></li>
                        <li><a href="v_faf.php">Frais au Forfait</a></li>
                        <li><a href="v_fhf.php">Frais hors Forfait</a></li>
                        <li><a href="v_consult.php">Consultation</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><form class="navbar-form navbar-right" method="post" action="../controller/c_homepage.php">  
                                <button type="submit" class="btn btn-default">Déconnexion</button>
                            </form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- FIN : Barre de navigation -->

        <!--DEBUT SOUS HEADER-->
        <div class="col-md-12">    
            <div class="row" id="encart_vert">
                <div class="col-sm-1">
                    <img id="picot-ss-head" src="../images/frais.jpg">
                </div>
                <div class="col-sm-2">
                    <h5 id="sous_head">Bonjour <?php echo $nom ?></h5>  
                </div>
                <div class="col-sm-9 text-right">
                    <h5 id="sous_head">Rôle : <?php echo $role ?></h5>
                </div>
            </div>
        </div> 
        <!--FIN SOUS HEADER-->

        <!--DEBUT BLOC TEXTE--> 
        <div class="col-md-6"  >
            <br>
            <div class="col-md-1 center-block" ></div>
            <div class="col-md-10 center-block">
                <h2 id="titre_FF">FRAIS AU FORFAIT</h2>     
                <div id="block_text_FF">
                    <p id="text_FF">
                        Nom | <?php echo $nom ?>
                    </p>
                    <p id="text_FF">
                        Prénom | <?php echo $prenom ?>
                    </p>
                </div>

                <h2 id="ss_titre_FF">Période d'engagement :</h2>     
                <div id="block_text_FF">
                    <p id="text_FF">
                        Mois | <?php echo date("m") ?>
                    </p>
                    <p id="text_FF">
                        Année | <?php echo date("Y") ?>
                    </p>
                </div>

                <div id="TRAIT"></div>
            </div>
            <div class="col-md-1 center-block" ></div>
        </div>
        <!--FIN BLOC TEXTE--> 

        <!-- Début block Ajout de Ligne Frais au Forfait -->
        <div class="col-md-6">
            <br>
            <div class="col-md-1 center-block" ></div>
            <h2 id="titre_FF">AJOUT FRAIS AU FORFAIT</h2>

            <form id="myForm" class="form-horizontal" action="../controller/c_faf.php" method="post">
                <fieldset>
                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" id="text_FF" for="selectbasic">Catégorie</label>
                        <div class="col-md-4">
                            <select id="selectbasic" name="typeFF" class="form-control">
                                <option value="ETP">Étape</option>
                                <option value="KM">Frais Kilométrique</option>
                                <option value="NUI">Nuitée Hôtel</option>
                                <option value="REP">Repas Restaurant</option>
                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" id="text_FF" for="qt">Quantité</label>  
                        <div class="col-md-4">
                            <input id="qt" name="qt" placeholder="" class="form-control input-md" > 
                            <!--avec Bootstrap et les caractéristiques suivantes type="number" required="", on peut éviter le script en java script-->
                        </div>
                        <span id="errorMsg" class="span"></span>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="insert"></label>
                        <div class="col-md-4">
                            <input type="submit" id="insert" name="insert" class="btn btn-success" value="Ajouter">
                            <input type="hidden" id="insert" name="insert">
                        </div>
                    </div>

                </fieldset>
            </form>

        </div>
        <!-- Fin block Ajout de Ligne Frais au Forfait -->

        <!-- DEBUT : TABLEAU FF -->
        <div class="row">
            <form role="form" name="form" action="../controller/c_faf.php" method="post">
                <table class="table table-striped table-responsive col-md-10 ">
                    <caption id="encart_vert">Frais au Forfait</caption>
                    <thead>
                        <tr>
                            <th>Mois</th>
                            <th>Type Frais</th>
                            <th>Quantité</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php foreach ($data as $row): ?>
                        <tr>
                            <td><?php echo $row['anneeMoisLigneFAF'] ?></td>
                            <td><?php echo $row['idFAF'] ?></td>
                            <td><?php echo $row['quantLigneFAF'] ?></td>
                            <td width=250>
                                <input type='submit'  name="update<?php echo $row['idligneFAF'] ?>" value='Modifier' class="btn btn-warning"/>
                                &nbsp;
                                <input type='submit'  name="delete<?php echo $row['idligneFAF'] ?>" value='Supprimer' class="btn btn-danger"/>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table> 
            </form>
        </div>
        <!-- FIN : TABLEAU FF -->

        <div id="BigBlanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : LOGO -->
        <div class="container logo" id="logo_page">    
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <img src="../images/universalpharmalogo.jpg" class="img-responsive" alt="Image">
                </div>
            </div>
        </div>
        <!-- FIN : LOGO -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : FOOTER -->
        <footer class="container-fluid" id="encart_footer">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">NOUS CONTACTER</h5>                  
                    <a href="#" id="footer_text">Contactez-nous par e-mail</a> 
                    <br>
                    <a href="#" id="footer_text">Contactez-nous par téléphone : +33 (0) 170489073</a>  
                    <br>
                    <a href="https://www.google.fr/maps/place/72+Quai+des+Carri%C3%A8res,+94220+Charenton-le-Pont/@48.8202625,2.4029738,17z/data=!3m1!4b1!4m5!3m4!1s0x47e672569eebee53:0x3b9f7e9b714d4683!8m2!3d48.820259!4d2.4051625" id="footer_text">Contactez-nous par courrier postal</a>                    
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">LIENS</h5>
                    <a  href="http://www.medicaments.social-sante.gouv.fr/" id="footer_text">médicaments.gouv.fr</a>
                    <br>
                    <a href="https://www.vidal.fr/" id="footer_text">vidal.fr</a>
                    <br>
                    <a href="http://www.ameli.fr/" id="footer_text">ameli.fr</a>
                    <br>
                    <a href="https://www.data.gouv.fr/fr/" id="footer_text">data.gouv.fr</a>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">RÉSEAUX SOCIAUX</h5>
                    <div id="picto_reso" class="col-sm-3"><a href='#'><img class="youtube"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="https://www.facebook.com/profile.php?id=100012325773575"><img class="fb"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="#"><img class="twitter"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href='#'><img class="linkedin"/></a></div>
                </div>
            </div>  
            <br>
            <div class="row" id="footer_MotionLegale">
                <div class="col-xs-12">
                </div>
                <p id="footer_texte_MentionLegale">Universal Pharma : 72, Quai des carrières Vitry/Seine. &copy 2008 UNIVERSAL PHARMA Tous droits réservés.</p>
            </div>
        </footer>
        <!-- FIN : FOOTER -->
    </body>
</html>
