<?php
session_start();
$nom = null;
$prenom = null;
$role = null;
$id = NULL;
if (isset($_SESSION['nom'])) {
    $nom = $_SESSION['nom'];
    $prenom = $_SESSION['prenom'];
    $role = $_SESSION['role'];
    $id = $_SESSION['id'];
}

include '../setup.php';
include'../dao/MyDao.php';

$dao = new MyDao();
$data = $dao->getUserById($id);
//var_dump($data);
//echo"YO";
//echo $data['mdpUtilisateur'];
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Universal Pharma</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/CSS" href="../third_party/bootstrap-3.3.6-dist/css/bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/csshomepage.css">
        <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    </head>
    <body class="row col-lg-10 col-lg-offset-1">
        <!-- DEBUT : Barre de navigation -->
        <!-- A enlever après test -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-brand">
                    <img id='uplogo' src='../images/universalpharmalogo.jpg' alt='uplogo'/>
                </div> 
                <div class="navbar-header">                
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="v_tabbord.php">Tableau de bord</a></li>
                        <li><a href="v_cptuser.php">Compte</a></li>
                        <li><a href="v_faf.php">Frais au Forfait</a></li>
                        <li><a href="v_fhf.php">Frais hors Forfait</a></li>
                        <li><a href="v_consult.php">Consultation</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><form class="navbar-form navbar-right" method="post" action="../controller/c_homepage.php">  
                                <button type="submit" class="btn btn-default">Déconnexion</button>
                            </form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- FIN : Barre de navigation -->

        <!--DEBUT SOUS HEADER-->
        <div class="col-md-12">    
            <div class="row" id="encart_vert">
                <div class="col-sm-1">
                    <img id="picot-ss-head" src="../images/compte.jpg">
                </div>
                <div class="col-sm-2">
                    <h5 id="sous_head">Bonjour <?php echo $nom ?></h5>  
                </div>
                <div class="col-sm-9 text-right">
                    <h5 id="sous_head">Rôle : <?php echo $role ?></h5>
                </div>
            </div>
        </div> 
        <!--FIN SOUS HEADER-->
        
      <!--DEBUT BLOC TEXTE--> 
        <div class="col-md-12 text-center"  >
            <br>
            <h2 id="titre_block-texte">Mise à jour de votre mot de passe</h2>
            <p id="block-texte">
                Mettez à jour votre mot de passe.<br>
                <br>
            </p>
        </div>
        <!--FIN BLOC TEXTE--> 

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : FORMULAIRE -->
        <form role="form" name="form" action="../controller/c_majco.php" method="post">
            <div class="col-md-12" >
                <div class="col-md-3 center-block" ></div>
                <div class="col-md-6 center-block" id="encart_vert">
                    <br>                  

                    <div class="form-group">
                        <label for="inputOldMdp" class="col-sm-4 control-label">Ancien mot de passe :</label>
                        <div class="col-sm-20">
                            <input type="text" name="inputOldMdp" class="form-control" id="inputOldMdp" placeholder="votre ancien mot de passe">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputNewMdp" class="col-sm-4 control-label">Nouveau mot de passe :</label>
                        <div class="col-sm-20">
                            <input type="text" name="inputNewMdp" class="form-control" id="inputNewMdp" placeholder="votre nouveau mot de passe">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="confirmationMdp" class="col-sm-4 control-label">Confirmation :</label>
                        <div class="col-sm-20">
                            <input type="text" name="confirmationMdp" class="form-control" id="confirmationMdp" placeholder="confirmation">
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-sm-24 center-block">
                            <button id="insert" name="updating" class="btn btn-success">Modifier</button>
                            <button type="button" onclick="self.location.href='../view/v_tabbord.php' "class="btn btn-inverse"><i class="icon icon-times icon-lg"></i> Retour</button>
                       
                       
                        </div>
                    </div>
                      <br>
                </div>
                <div class="col-md-3 center-block" ></div>
            </div>
        </form>
        <!-- FIN : FORMULAIRE -->   
        
        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        



        <div id="BigBlanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : LOGO -->
        <div class="container logo" id="logo_page">    
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <img src="../images/universalpharmalogo.jpg" class="img-responsive" alt="Image">
                </div>
            </div>
        </div>
        <!-- FIN : LOGO -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : FOOTER -->
        <footer class="container-fluid" id="encart_footer">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">NOUS CONTACTER</h5>                  
                    <a href="#" id="footer_text">Contactez-nous par e-mail</a> 
                    <br>
                    <a href="#" id="footer_text">Contactez-nous par téléphone : +33 (0) 170489073</a>  
                    <br>
                    <a href="https://www.google.fr/maps/place/72+Quai+des+Carri%C3%A8res,+94220+Charenton-le-Pont/@48.8202625,2.4029738,17z/data=!3m1!4b1!4m5!3m4!1s0x47e672569eebee53:0x3b9f7e9b714d4683!8m2!3d48.820259!4d2.4051625" id="footer_text">Contactez-nous par courrier postal</a>                    
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">LIENS</h5>
                    <a  href="http://www.medicaments.social-sante.gouv.fr/" id="footer_text">médicaments.gouv.fr</a>
                    <br>
                    <a href="https://www.vidal.fr/" id="footer_text">vidal.fr</a>
                    <br>
                    <a href="http://www.ameli.fr/" id="footer_text">ameli.fr</a>
                    <br>
                    <a href="https://www.data.gouv.fr/fr/" id="footer_text">data.gouv.fr</a>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">RÉSEAUX SOCIAUX</h5>
                    <div  class="col-sm-3"><a href='#'><img class="youtube"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="https://www.facebook.com/profile.php?id=100012325773575"><img class="fb"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="#"><img class="twitter"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href='#'><img class="linkedin"/></a></div>
                </div>
            </div>  
            <br>
            <div class="row" id="footer_MotionLegale">
                <div class="col-xs-12">
                </div>
                <p id="footer_texte_MentionLegale">Universal Pharma : 72, Quai des carrières Vitry/Seine. &copy 2008 UNIVERSAL PHARMA Tous droits réservés.</p>
            </div>
        </footer>
        <!-- FIN : FOOTER -->
    </body>
</html>


