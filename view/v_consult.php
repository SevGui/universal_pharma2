<?php
session_start();
$nom = null;
$prenom = null;
$role = null;
$id = NULL;
if (isset($_SESSION['nom'])) {
    $nom = $_SESSION['nom'];
    $prenom = $_SESSION['prenom'];
    $role = $_SESSION['role'];
    $id = $_SESSION['id'];
}

include '../setup.php';
include'../dao/MyDao.php';

$dao = new MyDao();
$dateD = date("Y") . date("m") - 1;
$dataFAF = $dao->getFAFByIdAnneeMois($id, $dateD);
$dataFHF = $dao->getFHFByIdAnneeMois($id, $dateD);
$dataFF = $dao->getFFByIdAnneeMois($id, $dateD);
//var_dump($dataFF);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Universal Pharma</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/CSS" href="../third_party/bootstrap-3.3.6-dist/css/bootstrap.css">
        <script src="../js/scriptConsult.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/csshomepage.css">
        <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    </head>
    <body class="row col-lg-10 col-lg-offset-1">
        <!-- DEBUT : Barre de navigation -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-brand">
                    <img id='uplogo' src='../images/universalpharmalogo.jpg' alt='uplogo'/>
                </div> 
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="v_tabbord.php">Tableau de bord</a></li>
                        <li><a href="v_cptuser.php">Compte</a></li>
                        <li><a href="v_faf.php">Frais au Forfait</a></li>
                        <li><a href="v_fhf.php">Frais hors Forfait</a></li>
                        <li><a href="v_consult.php">Consultation</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><form class="navbar-form navbar-right" method="post" action="../controller/c_homepage.php">  
                                <button type="submit" class="btn btn-default">Déconnexion</button>
                            </form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- FIN : Barre de navigation -->

        <!--DEBUT SOUS HEADER-->  
        <div class="col-md-12">    
            <div class="row" id="encart_vert">
                <div class="col-sm-1">
                    <img id="picot-ss-head" src="../images/loupe.jpg">
                </div>
                <div class="col-sm-2">
                    <h5 id="sous_head">Bonjour <?php echo $nom ?></h5>  
                </div>
                <div class="col-sm-9 text-right">
                    <h5 id="sous_head">Rôle : <?php echo $role ?></h5>
                </div>
            </div>
        </div> 
        <!--FIN SOUS HEADER-->  

        <!--DEBUT BLOC TEXTE--> 
        <div class="row">
            <div class="col-md-12 text-center"  >
                <br>
                <h2 id="titre_block-texte">Vos consultations</h2>
                <p id="block-texte">
                    Choisissez une période afin de consulter vos frais.              
                </p>            
            </div>
        </div>
        <!--FIN BLOC TEXTE--> 

        <!-- DEBUT : CHOIX DATE -->        
        <div class="container-fluid text-center">    
            <div class="row col-md-offset-4 col-md-4" id="encart_gris">             
                    <br>
                    <div class="form-group form-group-sm form-horizontal">
                        <label class="col-sm-5 control-label" for="dateC">Période</label>
                        <div class="col-sm-6">
                            <input class="form-control form-horizontal" placeholder="aaaamm" id="dateC" value="<?php echo $dateD ?>" onchange="consult(this.value)">
                        </div>
                        <span id="errorMsgDateC" class="span"></span>
                    </div>
                    <br>
            </div>
        </div>          
        <!-- FIN : CHOIX DATE -->
        
        <div class="col-md-12"></div>
        <div id="content">
        
        <!-- DEBUT : TABLEAU FAF -->
            <div class="row">
                <table class="table table-striped table-responsive col-md-10 ">
                    <caption id="encart_vert">Frais au Forfait</caption>
                    <thead>
                        <tr>
                            <th>Mois</th>
                            <th>Type Frais</th>
                            <th>Quantité</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataFAF as $row): ?>
                            <tr>
                                <td><?php echo $row['anneeMoisLigneFAF'] ?></td>
                                <td><?php echo $row['idFAF'] ?></td>
                                <td><?php echo $row['quantLigneFAF'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- FIN : TABLEAU FAF -->

            <!-- DEBUT : TABLEAU FHF -->
            <div class="row">
                <table class="table table-striped table-responsive col-md-10 ">
                    <caption id="encart_vert">Frais hors Forfait</caption>
                    <thead>
                        <tr>
                            <th>Mois</th>
                            <th>Libellé</th>
                            <th>Montant</th>
                            <th>Justificatif</th>
                            <th>Date Justificatif</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataFHF as $row): ?>
                            <tr>
                                <td><?php echo $row['anneeMoisLigneFHF'] ?></td>
                                <td><?php echo $row['libLigneFHF'] ?></td>
                                <td><?php echo $row['montLigneFHF'] ?></td>
                                <td><?php echo $row['scanJustiFHF'] ?></td>
                                <td><?php echo $row['dateJustiFHF'] ?></td> 
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- FIN : TABLEAU FHF -->

            <!-- DEBUT : TABLEAU FF -->
            <div class="row">
                <table class="table table-striped table-responsive col-md-10 ">
                    <caption id="encart_vert">Fiche de Frais</caption>
                    <thead>
                        <tr>
                            <th>Mois</th>
                            <th>nombre de Justificatifs</th>
                            <th>Véhicule</th>
                            <th>Date Modification</th>
                            <th>Montant</th>
                            <th>Status</th>      
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataFF as $row): ?>
                            <tr>
                                <td><?php echo $row['anneeMoisLigneFF'] ?></td>
                                <td><?php echo $row['nbJustifFF'] ?></td>
                                <td><?php echo $row['typeVehicule'] ?></td>
                                <td><?php echo $row['dateModifFF'] ?></td>
                                <td><?php echo $row['montantFF'] ?></td>
                                <td><?php echo $row['idStatFrais'] ?></td> 
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <!-- FIN : TABLEAU FF -->
        </div>
        
        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : LOGO -->
        <div class="container logo" id="logo_page">    
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <img src="../images/universalpharmalogo.jpg" class="img-responsive" alt="Image">
                </div>
            </div>
        </div>
        <!-- FIN : LOGO -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : FOOTER -->
        <footer class="container-fluid" id="encart_footer">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">NOUS CONTACTER</h5>                  
                    <a href="#" id="footer_text">Contactez-nous par e-mail</a> 
                    <br>
                    <a href="#" id="footer_text">Contactez-nous par téléphone : +33 (0) 170489073</a>  
                    <br>
                    <a href="https://www.google.fr/maps/place/72+Quai+des+Carri%C3%A8res,+94220+Charenton-le-Pont/@48.8202625,2.4029738,17z/data=!3m1!4b1!4m5!3m4!1s0x47e672569eebee53:0x3b9f7e9b714d4683!8m2!3d48.820259!4d2.4051625" id="footer_text">Contactez-nous par courrier postal</a>                    
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">LIENS</h5>
                    <a  href="http://www.medicaments.social-sante.gouv.fr/" id="footer_text">médicaments.gouv.fr</a>
                    <br>
                    <a href="https://www.vidal.fr/" id="footer_text">vidal.fr</a>
                    <br>
                    <a href="http://www.ameli.fr/" id="footer_text">ameli.fr</a>
                    <br>
                    <a href="https://www.data.gouv.fr/fr/" id="footer_text">data.gouv.fr</a>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">RÉSEAUX SOCIAUX</h5>
                    <div id="picto_reso" class="col-sm-3"><a href='#'><img class="youtube"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="https://www.facebook.com/profile.php?id=100012325773575"><img class="fb"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="#"><img class="twitter"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href='#'><img class="linkedin"/></a></div>
                </div>
            </div>  
            <br>
            <div class="row" id="footer_MotionLegale">
                <div class="col-xs-12">
                </div>
                <p id="footer_texte_MentionLegale">Universal Pharma : 72, Quai des carrières Vitry/Seine. &copy 2008 UNIVERSAL PHARMA Tous droits réservés.</p>
            </div>
        </footer>
        <!-- FIN : FOOTER -->
    </body>
</html>
