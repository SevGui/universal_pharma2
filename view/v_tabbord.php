<?php
session_start();
$nom = null;
$role = null;
if (isset($_SESSION['nom'])) {
    $nom = $_SESSION['nom'];
    $role = $_SESSION['role'];
}

include '../setup.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Universal Pharma</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/CSS" href="../third_party/bootstrap-3.3.6-dist/css/bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/csshomepage.css">
        <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    </head>
    <body class="row col-lg-10 col-lg-offset-1">
        <!-- DEBUT : Barre de navigation -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-brand">
                    <img id='uplogo' src='../images/universalpharmalogo.jpg' alt='uplogo'/>
                </div> 
                <div class="navbar-header">             
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="v_tabbord.php">Tableau de bord</a></li>
                        <li><a href="v_cptuser.php">Compte</a></li>
                        <li><a href="v_faf.php">Frais au Forfait</a></li>
                        <li><a href="v_fhf.php">Frais hors Forfait</a></li>
                        <li><a href="v_consult.php">Consultation</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><form class="navbar-form navbar-right" method="post" action="../controller/c_homepage.php">  
                                <button type="submit" class="btn btn-default">Déconnexion</button>
                            </form></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- FIN : Barre de navigation -->

        <!--DEBUT SOUS HEADER-->  
        <div class="col-md-12">    
            <div class="row" id="encart_vert">
                <div class="col-sm-1">
                    <img id="picot-ss-head" src="../images/TBBORD.png">
                </div>
                <div class="col-sm-2">
                    <h5 id="sous_head">Bonjour <?php echo $nom ?></h5>  
                </div>
                <div class="col-sm-9 text-right">
                    <h5 id="sous_head">Rôle : <?php echo $role ?></h5>
                </div>
            </div>
        </div> 
        <!--FIN SOUS HEADER-->  

        <!--DEBUT BLOC TEXTE-->      
        <div class="col-md-12 text-center"  >
            <br>
            <h2 id="titre_block-texte">Mon Tableau de bord.</h2>
            <p id="block-texte">
                Cette page vous permet : <br>
                . de modifier votre compte personnel.<br>
                . de traiter vos frais.<br>
                . de consulter vos frais.
            </p>
        </div>
        <!--FIN BLOC TEXTE--> 

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!--DEBUT PICTO TABLEAU DE BORD-->
        <div class="container" id="texte_picto">    
            <div class="row">
                <div class="col-sm-3 col-xs-3">  
                    <a href="v_cptuser.php"><img id="picto_tbbord" src="../images/compte.jpg" class="Compte" alt="Image"></a>
                    <br>
                    <p><b>Compte</b></p>
                </div>
                <div class="col-sm-3 col-xs-3">  
                    <a href="v_faf.php"><img id="picto_tbbord" src="../images/frais.jpg" class="Frais" alt="Image"></a>
                    <br>
                    <p><b>Frais au Forfait</b></p>
                </div>
                <div class="col-sm-3 col-xs-3">  
                    <a href="v_fhf.php"><img id="picto_tbbord" src="../images/frais.jpg" class="Frais" alt="Image"></a>
                    <br>
                    <p><b>Frais hors Forfait</b></p>
                </div>
                <div class="col-sm-3 col-xs-3"> 
                    <a href="v_consult.php"><img id="picto_tbbord" src="../images/loupe.jpg" class="Consultation" alt="Image"></a>
                    <br>
                    <p><b>Consultation</b></p>
                </div>             
            </div>
        </div>
        <!--FIN PICTO TABLEAU DE BORD-->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT CARROUSSEL -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="../images/photo_carousel_01.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>
                <div class="item">
                    <img src="../images/photo_carousel_02.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>
                <div class="item">
                    <img src="../images/photo_carousel_03.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>
                <div class="item">
                    <img src="../images/photo_carousel_04.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>
                <div class="item">
                    <img src="../images/photo_carousel_05.jpg" alt="Image" class='img-responsive'>
                    <div class="carousel-caption">
                    </div>      
                </div>
            </div>
            <!--             Left and right controls 
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>-->
        </div>
        <!-- FIN CARROUSSEL -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : LOGO -->
        <div class="container logo" id="logo_page">    
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <img src="../images/universalpharmalogo.jpg" class="img-responsive" alt="Image">
                </div>
            </div>
        </div>
        <!-- FIN : LOGO -->

        <div id="blanc" class="row"> 
            <div class="col-md-12"></div>               
        </div>

        <!-- DEBUT : FOOTER -->
        <footer class="container-fluid" id="encart_footer">
            <div class="row">
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">NOUS CONTACTER</h5>                  
                    <a href="#" id="footer_text">Contactez-nous par e-mail</a> 
                    <br>
                    <a href="#" id="footer_text">Contactez-nous par téléphone : +33 (0) 170489073</a>  
                    <br>
                    <a href="https://www.google.fr/maps/place/72+Quai+des+Carri%C3%A8res,+94220+Charenton-le-Pont/@48.8202625,2.4029738,17z/data=!3m1!4b1!4m5!3m4!1s0x47e672569eebee53:0x3b9f7e9b714d4683!8m2!3d48.820259!4d2.4051625" id="footer_text">Contactez-nous par courrier postal</a>                    
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">LIENS</h5>
                    <a  href="http://www.medicaments.social-sante.gouv.fr/" id="footer_text">médicaments.gouv.fr</a>
                    <br>
                    <a href="https://www.vidal.fr/" id="footer_text">vidal.fr</a>
                    <br>
                    <a href="http://www.ameli.fr/" id="footer_text">ameli.fr</a>
                    <br>
                    <a href="https://www.data.gouv.fr/fr/" id="footer_text">data.gouv.fr</a>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <h5 id="footer_titre">RÉSEAUX SOCIAUX</h5>
                    <div id="picto_reso" class="col-sm-3"><a href='#'><img class="youtube"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="https://www.facebook.com/profile.php?id=100012325773575"><img class="fb"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href="#"><img class="twitter"/></a></div>
                    <div id="picto_reso" class="col-sm-3"><a href='#'><img class="linkedin"/></a></div>
                </div>
            </div>  
            <br>
            <div class="row" id="footer_MotionLegale">
                <div class="col-xs-12">
                </div>
                <p id="footer_texte_MentionLegale">Universal Pharma : 72, Quai des carrières Vitry/Seine. &copy 2008 UNIVERSAL PHARMA Tous droits réservés.</p>
            </div>
        </footer>
        <!-- FIN : FOOTER -->
    </body>
</html>