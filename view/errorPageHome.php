<?php
session_start();
$msg = null;
if (isset($_SESSION['message'])) {
    $msg = $_SESSION['message'];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Universal Pharma</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/CSS" href="../third_party/bootstrap-3.3.6-dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/csshomepage.css">
        <link rel="icon" type="image/x-icon" href="../images/favicon.ico" />
    </head>
    <body>
        <div class="row col-lg-10 col-lg-offset-1">
            <!-- DEBUT : Barre de navigation -->
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-brand">
                        <img id='uplogo' src='../images/universalpharmalogo.jpg' alt='uplogo'/>
                    </div> 
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="nav navbar-nav"><a href="../index.php">Accueil</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- FIN : Barre de navigation --> 

            <!--DEBUT BLOC TEXTE--> 
            <div class="col-md-12 text-center"  >
                <br>
                <h2 id="titre_block-texte">Oups...</h2>
                <p id="block-texte">
                <div><?php echo $msg ?></div>             
                </p>
                <br>
            </div>
            <!--FIN BLOC TEXTE--> 

            <div id="blanc" class="row"> 
                <div class="col-md-12"></div>               
            </div>

            <!-- DEBUT : LOGO -->
            <div class="container logo" id="logo_page">    
                <div class="row">
                    <div class="col-sm-4 col-sm-push-4">
                        <img src="../images/universalpharmalogo.jpg" class="img-responsive" alt="Image">
                        <br>
                    </div>
                </div>
            </div>
            <!-- FIN : LOGO -->

            <!-- DEBUT : FOOTER -->
            <footer class="container-fluid" id="encart_footer">

                <div class="row" id="footer_MotionLegale">
                    <div class="col-xs-12">
                    </div>
                    <p id="footer_texte_MentionLegale">Universal Pharma : 72, Quai des carrières Vitry/Seine. &copy 2008 UNIVERSAL PHARMA Tous droits réservés.</p>
                </div>
            </footer>
            <!-- FIN : FOOTER -->
        </div>
    </body>
</html>