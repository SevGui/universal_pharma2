<?php

/**
 * MyDao classe static permet de se connecter à la bdd
 * et de traiter les requêtes Sql
 * @author Séverine
 */
class MyDao {

    private $datasource;
    private $user;
    private $password;
    private $conn;

    public function __construct() {
        $conf = parse_ini_file(INI_FILE, true);
        $settings = $conf['settings']['env'];
        $host = $conf[$settings]['host'];
        $dbName = $conf[$settings]['db_name'];
        $this->user = $conf[$settings]['user'];
        $this->password = $conf[$settings]['password'];
        $this->datasource = "mysql:host=$host;dbname=$dbName";
        $this->conn = new PDO($this->datasource, $this->user, $this->password
                , [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }

//    Méthodes pour l'utilisateur ____________________________________________

    /*
     * getUserByLogin
     * méthode qui permet de récupérer un utilisateur
     * en fonction de son login
     */
    public function getUserByLogin($login) {
        $query = "Select * from utilisateur natural join roles where loginUtilisateur=?";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($login));
        if ($sth->rowCount() === 0) {
            $resu = NULL;
        } else {
            $resu = $sth->fetch();
        }
        return $resu;
    }

    /*
     * getUserById
     * méthode qui permet de récupérer les informations d'un utilisateur 
     * en fonction de son id
     */
    public function getUserById($id) {
        $query = "Select * from utilisateur where idUtilisateur=?";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($id));
        if ($sth->rowCount() === 0) {
            $resu = NULL;
        } else {
            $resu = $sth->fetch();
        }
        return $resu;
    }
    
    /*
     * updateDataUser
     * méthode qui permet de modifier les données de l'utilisateur
     * en fonction de l'idUtilisateur
     * UPDATE UTILISATEUR SET `adressUtilisateur`= '9 rue bla', `cpUtilisateur` = '77483',`villUtilisateur` = 'fnznlefzn' where idUtilisateur = 'a131' 
     */
    public function updateDataUser($myUser) {
        $query = "UPDATE utilisateur SET"
                . " adressUtilisateur=:adress,"
                . " cpUtilisateur=:codeP,"
                . " villUtilisateur=:ville"
                . " WHERE idUtilisateur=:id";
        $q = $this->conn->prepare($query);
//        var_dump($query);
//        var_dump($myUser->getId());
        $q->execute(array(
            ':adress' => $myUser->getAdresse(),
            ':codeP' => $myUser->getCp(),
            ':ville' => $myUser->getVille(),
                ':id' => $myUser->getId()));
//            var_dump($q);
        return TRUE;
    }
    
    /*
     * updateMdpUser
     * méthode qui permet de modifier le mdp l'utilisateur
     * en fonction de l'idUtilisateur
     */
    public function updateMdpUser($myUser) {
//        echo 'die';
//        die;
        $query = "UPDATE utilisateur SET"
                . " mdpUtilisateur=:inputNewMdp"
                . " WHERE idUtilisateur=:id";
        $q = $this->conn->prepare($query);
//        var_dump($query);
//        die;
//        var_dump($myUser->getId());
//        die;
        $q->execute(array(
            ':inputNewMdp' => $myUser->getMdp(),
                ':id' => $myUser->getId()));
//            var_dump($q);
//            die;
        return TRUE;
    }
    
    /*
     * incrementLogAttempt
     * fonction qui permet d'incrémenter de 1 dans la BDD
     * si mdp mauvais
     */

    public function incrementLogAttempt(user $user) {
        $query = "UPDATE UTILISATEUR set `logAttempt`=`logAttempt` + 1 where idUtilisateur=:id";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $success = $sth->execute(array(':id' => $user->getId()));
        if ($success && $sth->rowCount() !== 0) {
            $log = $user->getLogAttempt() + 1;
            $user->setLogAttempt($log);
        }
//        else {
//            // TODO: Display une erreur, gestion des errors centralisé ?
//        }
    }

    //    Méthodes pour ligne frais au forfait ____________________________________________

    /*
     * getFAFByIdAnneeMois
     * méthode qui permet de récupérer une ligne frais au forfait
     * en fonction de l'année et du mois ainsi que l'id 
     * de l'utilisateur passé en argument
     */

    public function getFAFByIdAnneeMois($id, $dateD = NULL) {
        if ($dateD == NULL){
            $today = date("Ym");
        }else{
            $today = $dateD;
        }
        $query = "SELECT idligneFAF, anneeMoisLigneFAF, idFAF, quantLigneFAF FROM lignefraisauforfait WHERE idUtilisateur =? and anneeMoisLigneFAF =?";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($id, $today));
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }

    /*
     * getFAFLigne
     * méthode qui permet de récupérer une ligne frais au forfait
     * en fonction de l'id de la ligne frais au forfait 
     */

    public function getFAFLigne($idLFAF) {
        $query = "SELECT idligneFAF, anneeMoisLigneFAF, idFAF, quantLigneFAF FROM lignefraisauforfait WHERE idligneFAF=:idLFAF";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array(':idLFAF' => $idLFAF));
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }

    /*
     * insertDataFAF
     * méthode qui permet d'insérer une ligne frais au forfait
     */

    public function insertDataFAF(FAF $myFAF) {
        $query = "INSERT INTO lignefraisauforfait SET idUtilisateur=:user,"
                . " anneeMoisLigneFAF=:lemois,"
                . " idFAF=:typeFF,"
                . " quantLigneFAF=:qt";
        $q = $this->conn->prepare($query);
        $q->execute(array(':user' => $myFAF->getIdUtilisateur(),
            ':lemois' => $myFAF->getAnneeMois(),
            ':typeFF' => $myFAF->getIdFAF(),
            ':qt' => $myFAF->getQuantite()));
        return TRUE;
    }

    /*
     * updateDataFAF
     * méthode qui permet de modifier une ligne frais au forfait
     * en fonction de l'id ligne frais au forfait
     */

    public function updateDataFAF(FAF $myFAF) {
        $query = "UPDATE lignefraisauforfait SET idUtilisateur=:user,"
                . " anneeMoisLigneFAF=:lemois,"
                . " idFAF=:typeFF,"
                . " quantLigneFAF=:qt"
                . " WHERE idligneFAF=:idLFAF";
        $q = $this->conn->prepare($query);
        $q->execute(array(':user' => $myFAF->getIdUtilisateur(),
            ':lemois' => $myFAF->getAnneeMois(),
            ':typeFF' => $myFAF->getIdFAF(),
            ':qt' => $myFAF->getQuantite(),
            ':idLFAF' => $myFAF->getIdLFAF()));
        return TRUE;
    }

    /*
     * deleteDataFAF
     * méthode qui permet de supprimer une ligne frais au forfait
     * en fonction de l'id ligne frais au forfait
     */

    public function deleteDataFAF($idLFAF) {
        $sql = "DELETE FROM lignefraisauforfait WHERE idligneFAF=:idLFAF";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':idLFAF' => $idLFAF));
        return TRUE;
    }

    //    Méthodes pour ligne frais hors forfait ____________________________________________

    /*
     * getFHFByIdAnneeMois
     * méthode qui permet de récupérer une ligne frais hors forfait
     * en fonction de l'année et du mois ainsi que l'id 
     * de l'utilisateur passé en argument
     */
    public function getFHFByIdAnneeMois($id,$dateD = NULL) {
        if ($dateD == NULL){
            $today = date("Ym");
        }else{
            $today = $dateD;
        }
        $query = "SELECT idligneFHF, anneeMoisLigneFHF, libLigneFHF, dateJustiFHF, montLigneFHF, scanJustiFHF FROM lignefraishorsforfait WHERE idUtilisateur =? and anneeMoisLigneFHF =?";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($id, $today));
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }

    /*
     * insertDataFHF
     * méthode qui permet d'insérer une ligne frais hors forfait
     */

    public function insertDataFHF(FHF $myFHF) {
        $query = "INSERT INTO lignefraishorsforfait SET idUtilisateur=:user,"
                . " anneeMoisLigneFHF=:lemois,"
                . " libLigneFHF=:typeFF,"
                . " dateJustiFHF=:datej,"
                . " montLigneFHF=:montant,"
                . " scanJustiFHF=:scan";
        $q = $this->conn->prepare($query);
        $q->execute(array(':user' => $myFHF->getIdUtilisateur(),
            ':lemois' => $myFHF->getAnneeMois(),
            ':typeFF' => $myFHF->getLibelle(),
            ':datej' => $myFHF->getDateJustif(),
            ':montant' => $myFHF->getMontant(),
            ':scan' => $myFHF->getScan()));
        return TRUE;
    }

    /*
     * deleteDataFHF
     * méthode qui permet de supprimer une ligne frais hors forfait
     * en fonction de l'id ligne frais hors forfait
     */

    public function deleteDataFHF($idLFHF) {
        $sql = "DELETE FROM lignefraishorsforfait WHERE idligneFHF=:idLFHF";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':idLFHF' => $idLFHF));
        return TRUE;
    }

    /*
     * getFHFLigne
     * méthode qui permet de récupérer une ligne frais hors forfait
     * en fonction de l'id de la ligne frais hors forfait 
     */

    public function getFHFLigne($idLFHF) {
        $query = "SELECT idligneFHF, anneeMoisLigneFHF, libLigneFHF, dateJustiFHF, montLigneFHF, scanJustiFHF FROM lignefraishorsforfait WHERE idligneFHF=:idLFHF";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array(':idLFHF' => $idLFHF));
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }

    /*
     * updateDataFHF
     * méthode qui permet de modifier une ligne frais hors forfait
     * en fonction de l'id ligne frais hors forfait
     */

    public function updateDataFHF(FHF $myFHF) {
        $query = "UPDATE lignefraishorsforfait SET idUtilisateur=:user,"
                . " anneeMoisLigneFHF=:lemois,"
                . " libLigneFHF=:typeFF,"
                . " dateJustiFHF=:datej,"
                . " montLigneFHF=:montant,"
                . " scanJustiFHF=:scan"
                . " WHERE idligneFHF=:idLFHF";
        $q = $this->conn->prepare($query);
        $q->execute(array(':user' => $myFHF->getIdUtilisateur(),
            ':lemois' => $myFHF->getAnneeMois(),
            ':typeFF' => $myFHF->getLibelle(),
            ':datej' => $myFHF->getDateJustif(),
            ':montant' => $myFHF->getMontant(),
            ':scan' => $myFHF->getScan(),
            ':idLFHF' => $myFHF->getIdLFHF()));
        return TRUE;
    }
    
     /*
     * getFHFMaxIdLFHF
     * méthode qui permet de récupérer l'idLigneFHF maximum et
     * de le lui rajouter 1
     */

    public function getFHFMaxIdLFHF() {
        $query = "SELECT MAX(idLigneFHF) as 'MAX' FROM lignefraishorsforfait ";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array());
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }
    
//    Méthodes pour fiche de frais ____________________________________________

    /*
     * getFFByIdAnneeMois
     * méthode qui permet de récupérer une fiche de frais
     * en fonction de l'année et du mois ainsi que l'id 
     * de l'utilisateur passé en argument
     */

    public function getFFByIdAnneeMois($id, $dateD = NULL) {
        if ($dateD == NULL){
            $today = date("Ym");
        }else{
            $today = $dateD;
        }
        $query = "SELECT anneeMoisLigneFF, nbJustifFF, typeVehicule, dateModifFF, montantFF, idStatFrais FROM fichedefrais NATURAL JOIN vehicule WHERE idUtilisateur =? and anneeMoisLigneFF =?";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($id, $today));
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }    

}
