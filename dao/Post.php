<?php

/**
 * Description of POST
 * Permet de récupérer les informations entre les différentes pages par POST
 * @author sev
 */
class Post {

    public static function myPOST($myVariable) {
        /*
         * filter_input returns: 
         * Value of the requested variable on success, 
         * FALSE if the filter fails, or 
         * NULL if the variable_name variable is not set
         */
        return (NULL == (filter_input(INPUT_POST, $myVariable))) ?
                FALSE :
                filter_input(INPUT_POST, $myVariable, FILTER_SANITIZE_STRING);
    }

    public static function allPost() {
        return filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    }

    public static function myFile($fichier) {

        // Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur
        if (isset($fichier) AND $fichier['error'] == 0) {
            // Testons si le fichier n'est pas trop gros
            if ($fichier['size'] <= 1000000) {
                // Testons si l'extension est autorisée
                $infosfichier = pathinfo($fichier['name']);
                $extension_upload = $infosfichier['extension'];
                $extensions_autorisees = array('jpg', 'jpeg', 'pdf');
                if (in_array($extension_upload, $extensions_autorisees)) {

                    // On peut valider le fichier et le stocker définitivement
//                    move_uploaded_file($fichier['tmp_name'], 'UPLOADS' . basename($fichier['name']));
                    return true;
                } else {
                    $msg = '<p><strong>Fichier avec mauvaise extension</strong></p>';
                    return $msg;
                }
            } else {
                $msg = '<p><strong>Fichier trop important</strong></p>';
                return $msg;
            }
        } else if (!isset($fichier) == FALSE) {
            return FALSE;
        } else {
            $msg = '<p><strong>Erreur du fichier</strong></p>';
            return $msg;
        }
    }

    public static function myPOST_INT($myVariable) {

        return (NULL == (filter_input(INPUT_POST, $myVariable))) ?
                FALSE :
                filter_input(INPUT_POST, $myVariable, FILTER_VALIDATE_REGEXP,
                        ['options' => ['regexp' => '/^[0-9]{1,4}$/']]);
    }

    public static function myPOST_Date($myVariable) {

        $date = self::myPOST($myVariable);
        $date = DateTime::createFromFormat('Ym', $date);
        if (!$date) {
            return FALSE;
        } else {
            return $date->format('Ym');
        }
    }

    public static function myPOST_DEC($myVariable) {

        return (NULL == (filter_input(INPUT_POST, $myVariable))) ?
                FALSE :
                filter_input(INPUT_POST, $myVariable, FILTER_VALIDATE_REGEXP,
                        ['options' => ['regexp' => '/^[0-9]{1,5}.?[0-9]{0,2}?$/']]);
    }
    
    public static function myPOST_DateJ($myVariable) {

        $date = self::myPOST($myVariable);
//        var_dump($date);
        $date = DateTime::createFromFormat('Y-m-d', $date);
//        var_dump($date);
//        exit();
        if (!$date) {
            return FALSE;
        } else {
            return $date->format('Y-m-d');
        }
    }

}
