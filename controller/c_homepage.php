<?php
session_start();

include '../setup.php';
include '../entity/User.class.php';
include '../dao/Post.php';
include '../dao/MyDao.php';

if (Post::myPOST('login') !== FALSE && Post::myPOST('password') !== FALSE) {

    $user = Post::myPOST('login');
    $password = sha1(Post::myPOST('password'));

    $dao = new MyDao();
    $resu = $dao->getUserByLogin($user);
//    var_dump($resu);
//    
    //si il n'y a pas de User j'affiche msg d'erreur
    if ($resu == NULL) {
        $_SESSION['message'] = '<p><strong>Utilisateur non trouvé !</strong></p>';
        header("Location:../view/errorPageHome.php");
    } else {
        //sinon je demarre une new session lié au User
        $util1 = new user($resu["idUtilisateur"], $resu["nomUtilisateur"], $resu["prenomUtilisateur"], $resu["loginUtilisateur"], $resu["mdpUtilisateur"], $resu["adressUtilisateur"], $resu["cpUtilisateur"], $resu["villUtilisateur"], $resu["datEmbauche"], $resu["civUtilisateur"], $resu["mailUtilisateur"], $resu["etatUtilisateur"], $resu["idroles"], $resu["libRoles"], $resu["logAttempt"]);
        
//        var_dump($util1);
//        die();

        $_SESSION['nom'] = $util1->getNom();
        $_SESSION['prenom'] = $util1->getPrenom();
        $_SESSION['role'] = $util1->getRole();
        $_SESSION['id'] = $util1->getId();
        
        //test Autentication
        $resAuth = $util1->checkAutentication($password);
//        var_dump($resAuth);
//        die();
        
        //si le test ne retourne pas d'erreur => redirige vers le tabbord
        if (!$resAuth['error']) {
            header("Location:../view/v_tabbord.php");
        } else {
            //sinon msg erreur 
            $_SESSION['message'] = '<p class="text-center"><strong>' . $resAuth['error'] . '</strong></p>';
            header("Location:../view/errorPageHome.php");
        }
    }

} else {
    session_destroy();
    header("Location:../index.php");
}

