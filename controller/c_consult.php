<?php
session_start();

$id = NULL;
if (isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
}
include '../setup.php';
include'../dao/MyDao.php';

$anneeMois = (isset($_GET["anneemois"])) ? $_GET["anneemois"] : NULL;
//var_dump($anneeMois);
if ($anneeMois !== NULL) {
    $dao = new MyDao();
    $dataFAF = $dao->getFAFByIdAnneeMois($id, $anneeMois);
    $dataFHF = $dao->getFHFByIdAnneeMois($id, $anneeMois);
    $dataFF = $dao->getFFByIdAnneeMois($id, $anneeMois);
}
?>
<!-- DEBUT : TABLEAU FAF -->
<div class="row">
    <table class="table table-striped table-responsive col-md-10 ">
        <caption id="encart_vert">Frais au Forfait</caption>
        <thead>
            <tr>
                <th>Mois</th>
                <th>Type Frais</th>
                <th>Quantité</th>
            </tr>
        </thead>
        <tbody>
<?php foreach ($dataFAF as $row): ?>
                <tr>
                    <td><?php echo $row['anneeMoisLigneFAF'] ?></td>
                    <td><?php echo $row['idFAF'] ?></td>
                    <td><?php echo $row['quantLigneFAF'] ?></td>
                </tr>
<?php endforeach; ?>
        </tbody>
    </table>
</div>
<!-- FIN : TABLEAU FAF -->
<!-- DEBUT : TABLEAU FHF -->
<div class="row">
    <table class="table table-striped table-responsive col-md-10 ">
        <caption id="encart_vert">Frais hors Forfait</caption>
        <thead>
            <tr>
                <th>Mois</th>
                <th>Libellé</th>
                <th>Montant</th>
                <th>Justificatif</th>
                <th>Date Justificatif</th>
            </tr>
        </thead>
        <tbody>
<?php foreach ($dataFHF as $row): ?>
                <tr>
                    <td><?php echo $row['anneeMoisLigneFHF'] ?></td>
                    <td><?php echo $row['libLigneFHF'] ?></td>
                    <td><?php echo $row['montLigneFHF'] ?></td>
                    <td><?php echo $row['scanJustiFHF'] ?></td>
                    <td><?php echo $row['dateJustiFHF'] ?></td> 
                </tr>
<?php endforeach; ?>
        </tbody>
    </table>
</div>
<!-- FIN : TABLEAU FHF -->

<!-- DEBUT : TABLEAU FF -->
<div class="row">
    <table class="table table-striped table-responsive col-md-10 ">
        <caption id="encart_vert">Fiche de Frais</caption>
        <thead>
            <tr>
                <th>Mois</th>
                <th>nombre de Justificatifs</th>
                <th>Véhicule</th>
                <th>Date Modification</th>
                <th>Montant</th>
                <th>Status</th>      
            </tr>
        </thead>
        <tbody>
<?php foreach ($dataFF as $row): ?>
                <tr>
                    <td><?php echo $row['anneeMoisLigneFF'] ?></td>
                    <td><?php echo $row['nbJustifFF'] ?></td>
                    <td><?php echo $row['typeVehicule'] ?></td>
                    <td><?php echo $row['dateModifFF'] ?></td>
                    <td><?php echo $row['montantFF'] ?></td>
                    <td><?php echo $row['idStatFrais'] ?></td> 
                </tr>
<?php endforeach; ?>
        </tbody>
    </table>
</div>
<!-- FIN : TABLEAU FF -->
