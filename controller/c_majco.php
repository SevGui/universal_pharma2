<?php

session_start();
$nom = null;
$prenom = null;
$role = null;
$id = NULL;

if (isset($_SESSION['nom'])) {
    $nom = $_SESSION['nom'];
    $prenom = $_SESSION['prenom'];
    $role = $_SESSION['role'];
    $id = $_SESSION['id'];
}
include_once '../setup.php';
include '../dao/Post.php';
include '../entity/User.class.php';
include '../dao/MyDao.php';

/*
 * Méthode qui permet de :
 * -> comparer l'ancien mot de passe avec la BDD
 * -> comparer le nouveau mot de passe avec l'ancien mot de passe s'il ne sont pas égaux
 * -> comparer si le nouveau mot de passe est le même que la confirmation 
 * -> puis de renvoyer la requête updateMdpUser
 */

//on instancie MyDao
$dao = new MyDao();

$action = array('updating');

//récupère l'ID
$data = $dao->getUserById($id);
//var_dump($data);
/*
 * Déclaration de la variable $mdpBdd
 * qui récupère le mt de passe de l'utilisateur
 */
$mdpBdd = $data['mdpUtilisateur'];

//on récupère les post
$post = Post::allPost();
//var_dump($post);

/*
 * Déclaration de la variable $oldMdp (ancien mdp)encryptée
 */
$oldMdp = sha1($_POST['inputOldMdp']);

/*
 * Déclaration de la variable $newMdp (nouveau mdp)encryptée
 */
$newMdp = sha1($_POST['inputNewMdp']);

/*
 * Déclaration de la variable $confirmMdp (confirmation mdp)encryptée
 */
$confirmMdp = sha1($_POST['confirmationMdp']);

foreach ($post as $key => $value) {
    foreach ($action as $actionValue) {
        if (strpos($key, $actionValue) !== false) {
            $myret['action'] = $actionValue;
            $myret['index'] = filter_var($key, FILTER_SANITIZE_NUMBER_INT);
        }
    }
}

/*
 * Condition : si les champs inputOldMdp + inputNewMdp + confirmationMdp
 * ne sont pas remplis -> renvoie un message d'erreur
 */

switch ($myret['action']) {

    case 'updating':
//        echo 'modification2';
        if (Post::myPOST('inputOldMdp') === FALSE || Post::myPOST('inputNewMdp') === FALSE || Post::myPOST('confirmationMdp') === FALSE) {
            $msg = '<p><strong>Merci de remplir tous les champs du formulaire</strong></p>';
            include_once '../view/errorPage.php';
            //header("Location:../view/errorPage.php");
            /*
             * Sinon si le nouveau mdp correspond au mdp de la BDD
             * renvoie un message d'erreur
             */
        } elseif ($newMdp === $mdpBdd) {
            $msg = '<p><strong>Votre Nouveau mot de passe est identique à l\'ancien</strong></p>';
            //include_once '../view/errorPage.php';
            header("Location:../view/errorPage.php");
            /*
             * sinon si le nouveau mdp de correspond pas à la confirmation
             * renvoie un message d'erreur
             */
        } elseif ($newMdp !== $confirmMdp) {
            $msg = '<p><strong>Votre confirmation de mot de passe ne coîncide pas avec votre nouveau mot de passe</strong></p>';
            //include_once '../view/errorPage.php';
            header("Location:../view/errorPage.php");
            /*
             * Sinon si le mdp actuel ne correspond pas avec le mdp de la base de donnée
             * renvoie un message d'erreur
             */
        } elseif ($oldMdp !== $mdpBdd) {
            $msg = '<p><strong>Votre mot de passe actuel est incorrecte</strong></p>';
            //include_once '../view/errorPage.php';
            header("Location:../view/errorPage.php");
            /*
             * Sinon si le mdp actuel correspond avec le mdp de la BDD
             * et que le nuveau mdp correspond avec la confirmation
             * et que le nouveau mdp est différent du nuveau mdp
             * renvoi la requête updateMdpUser
             */
        } elseif ($mdpBdd === $oldMdp || $newMdp === $confirmMdp || $newMdp !== $mdpBdd || Post::myPOST('inputNewMdp') !== FALSE) {
            $mdp = sha1($_POST['inputNewMdp']);
            $myUser = new user($id, $nom, $prenom, $data['loginUtilisateur'], $mdp, $data['adressUtilisateur'], 
                    $data['cpUtilisateur'], $data['villUtilisateur'], $data['datEmbauche'], $data['civUtilisateur'], 
                    $data['mailUtilisateur'], $data['etatUtilisateur'], $data['idroles'], $role, $data['logAttempt']);
//            var_dump($myUser);
//            die();
            $resu = $dao->updateMdpUser($myUser);
//            var_dump($resu);
//            die();
            if ($resu === TRUE) {
                header("Location:../view/v_tabbord.php");
            } else {
                $msg = '<p><strong>Erreur avec la base de Données</strong></p>';
                header("Location:../view/errorPage.php");
                //include_once '../view/errorPage.php';
            }
        } else {
            header("Location:../view/v_tabbord.php");
        }

        break;
}



