<?php

session_start();
$id = NULL;
$idLFAF = NULL;
if (isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $idLFAF = $_SESSION['idLFAF'];
}

include '../setup.php';
include '../dao/Post.php';
include '../entity/FAF.class.php';
include '../dao/MyDao.php';

$dao = new MyDao();
$action = array('insert', 'update', 'updating', 'delete');

$post = Post::allPost();
//var_dump($post);
//die();

foreach ($post as $key => $value) {
    foreach ($action as $actionValue) {
        if (strpos($key, $actionValue) !== false) {
            $myret['action'] = $actionValue;
            $myret['index'] = filter_var($key, FILTER_SANITIZE_NUMBER_INT);
        }
    }
}

//var_dump($myret);
//die();

switch ($myret['action']) {
    case 'insert':
//        echo 'insertion';

        $today = date("Ym");
        if (Post::myPOST('typeFF') !== FALSE || Post::myPOST('qt') !== FALSE) {
            $idFAF = Post::myPOST('typeFF');
            if (Post::myPOST_INT('qt') !== FALSE) {
                $quantite = Post::myPOST_INT('qt');

                $myfaf = new FAF($id, $today, $idFAF, $quantite);
//            var_dump($myfaf);
                $resu = $dao->insertDataFAF($myfaf);
//            var_dump($resu);
            } else {
                header("Location:../view/v_faf.php");
            }

            if ($resu === TRUE) {
                header("Location:../view/v_faf.php");
            } else {
                $msg = '<p><strong>Erreur avec la base de Données</strong></p>';
                include_once '../view/errorPage.php';
            }
        } else {
            header("Location:../view/v_faf.php");
        }

        break;

    case 'update':
//        echo 'modification';

        $_SESSION['idLFAF'] = $myret['index'];
        header("Location:../view/v_faf_maj.php");

        break;

    case 'updating':
//        echo 'modification2';
        if (Post::myPOST('typeFF') !== FALSE || Post::myPOST('qt') !== FALSE || Post::myPOST('mois')) {
            $idFAF = Post::myPOST('typeFF');
            
            if (Post::myPOST_INT('qt') !== FALSE || Post::myPOST_Date('mois') !== FALSE) {
                $quantite = Post::myPOST_INT('qt');
                $anneeMois = Post::myPOST_Date('mois');
                $myfaf = new FAF($id, $anneeMois, $idFAF, $quantite, $idLFAF);

                $resu = $dao->updateDataFAF($myfaf);
//        var_dump($resu);
//        die();
            } else {
                header("Location:../view/v_faf.php");
            }


            if ($resu === TRUE) {
                header("Location:../view/v_faf.php");
            } else {
                $msg = '<p><strong>Erreur avec la base de Données</strong></p>';
                include_once '../view/errorPage.php';
            }
        } else {
            header("Location:../view/v_faf.php");
        }

        break;

    case 'delete':
//        echo 'suppression';
//        echo $myret['index'];
//        die();

        $resu = $dao->deleteDataFAF($myret['index']);
//        echo $resu;

        if ($resu === TRUE) {
            header("Location:../view/v_faf.php");
        } else {
            $msg = '<p><strong>Erreur avec la base de Données</strong></p>';
            include_once '../view/errorPage.php';
        }
        break;
}

