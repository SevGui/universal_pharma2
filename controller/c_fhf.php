<?php

session_start();
$id = NULL;
$idLFHF = NULL;
if (isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    if (isset($_SESSION['idLFHF'])){
        $idLFHF = $_SESSION['idLFHF'];
    }
}

include '../setup.php';
include '../dao/Post.php';
include '../entity/FHF.class.php';
include '../dao/MyDao.php';
include '../utile/FctUtil.php';

$dao = new MyDao();
$action = array('insert', 'update', 'updating', 'delete');

$post = Post::allPost();

//var_dump($_FILES['scanjustif']);
//var_dump($post);
//die();

foreach ($post as $key => $value) {
    foreach ($action as $actionValue) {
        if (strpos($key, $actionValue) !== false) {
            $myret['action'] = $actionValue;
            $myret['index'] = filter_var($key, FILTER_SANITIZE_NUMBER_INT);
        }
    }
}

//var_dump($myret);
//die();

switch ($myret['action']) {
    case 'insert':
//        echo 'insertion';
        $fichier = $_FILES['scanjustif'];
        $today = date("Ym");
        if (Post::myPOST('libelle') !== FALSE || Post::myPOST('montant') !== FALSE) {
            $resultat = Post::myFile($fichier);
            switch ($resultat) {
                case TRUE :

                    if (Post::myPOST_DEC('montant') !== FALSE || Post::myPOST_DateJ('datej')) {
                        $montant = Post::myPOST_DEC('montant');
                        $dateJustif = Post::myPOST_DateJ('datej');
                        $scan = FctUtil::reName($fichier, $id);
                        $libelle = Post::myPOST('libelle');

                        $myfhf = new FHF($id, $today, $libelle, $montant, $dateJustif, $scan);
//                    var_dump($myfhf);
//                    die();
                        $resu = $dao->insertDataFHF($myfhf);
                        $record = FctUtil::recordFile($fichier, $scan);
//                    var_dump($record);
//                    die();
                    } else {
                        header("Location:../view/v_fhf.php");
                    }

                    if ($resu === TRUE and $record == TRUE) {
                        header("Location:../view/v_fhf.php");
                    } else {
                        $msg = '<p><strong>Erreur avec la base de Données</strong></p>';
                        include_once '../view/errorPage.php';
                    }
                    break;

                case FALSE :

                    if (Post::myPOST_DEC('montant') !== FALSE || Post::myPOST_DateJ('datej')) {
                        $montant = Post::myPOST_DEC('montant');
                        $dateJustif = Post::myPOST_DateJ('datej');
                        $scan = "";
                        $libelle = Post::myPOST('libelle');

                        $myfhf = new FHF($id, $today, $libelle, $montant, $dateJustif, $scan);
//                var_dump($myfhf);
//                die();
                        $resu = $dao->insertDataFHF($myfhf);
                    } else {
                        header("Location:../view/v_fhf.php");
                    }

                    if ($resu === TRUE) {
                        header("Location:../view/v_fhf.php");
                    } else {
                        $msg = '<p><strong>Erreur avec la base de Données</strong></p>';
                        include_once '../view/errorPage.php';
                    }
                    break;

                default :
                    $msg = $resultat;
                    include_once '../view/errorPage.php';
                    break;
            }
        } else {
            header("Location:../view/v_fhf.php");
        }

        break;

    case 'update':
//        echo 'modification';

        $_SESSION['idLFHF'] = $myret['index'];
        header("Location:../view/v_fhf_maj.php");

        break;

    case 'updating':
//        echo 'modification2';

        $fichier = $_FILES['scanjustif'];

        if (Post::myPOST('libelle') !== FALSE || Post::myPOST('montant') !== FALSE || Post::myPOST('mois')) {
            $resultat = Post::myFile($fichier);
            switch ($resultat) {
                case TRUE :
                    
                    if (Post::myPOST_DEC('montant') !== FALSE || Post::myPOST_DateJ('datej') || Post::myPOST_Date('mois')) {
                        $scan = FctUtil::updateName($fichier, $id, $idLFHF);
                        $montant = Post::myPOST_DEC('montant');
                        $dateJustif = Post::myPOST_DateJ('datej');
                        $libelle = Post::myPOST('libelle');
                        $anneeMois = Post::myPOST_Date('mois');

                        $myfhf = new FHF($id, $anneeMois, $libelle, $montant, $dateJustif, $scan, $idLFHF);
//                                    var_dump($myfhf);
//                                    die();

                        $resu = $dao->updateDataFHF($myfhf);
                        $record = FctUtil::recordFile($fichier, $scan);
                        //            var_dump($resu);
                        //            die();
                    } else {
                        header("Location:../view/v_fhf.php");
                    }

                    if ($resu === TRUE and $record == TRUE) {
                        header("Location:../view/v_fhf.php");
                    } else {
                        $msg = '<p><strong>Erreur avec la base de Données</strong></p>';
                        include_once '../view/errorPage.php';
                    }
                    break;
                case FALSE :
                    if (Post::myPOST('scanjustif1') == FALSE) {
                        $scan = "";
                    } else {
                        $scan = Post::myPOST('scanjustif1');
                    }
                    if (Post::myPOST_DEC('montant') !== FALSE || Post::myPOST_DateJ('datej') || Post::myPOST_Date('mois')) {
                        $libelle = Post::myPOST('libelle');
                        $montant = Post::myPOST_DEC('montant');
                        $dateJustif = Post::myPOST_DateJ('datej');
                        $anneeMois = Post::myPOST_Date('mois');

                        $myfhf = new FHF($id, $anneeMois, $libelle, $montant, $dateJustif, $scan, $idLFHF);
                        //            var_dump($myfhf);
                        //            die();

                        $resu = $dao->updateDataFHF($myfhf);
                        //            var_dump($resu);
                        //            die();
                    } else {
                        header("Location:../view/v_fhf.php");
                    }

                    if ($resu === TRUE) {
                        header("Location:../view/v_fhf.php");
                    } else {
                        $msg = '<p><strong>Erreur avec la base de Données</strong></p>';
                        include_once '../view/errorPage.php';
                    }
                    break;
                default :
                    $msg = $resultat;
                    include_once '../view/errorPage.php';
                    break;
            }
        } else {
            header("Location:../view/v_fhf.php");
        }

        break;

    case 'delete':
//        echo 'suppression';
//        echo $myret['index'];

        $resu = $dao->deleteDataFHF($myret['index']);
        $record = FctUtil::deleteFile($myret['index'], $id);
//        echo $record;
//        die();

        if ($resu === TRUE and $record == TRUE) {
            header("Location:../view/v_fhf.php");
        } else {
            $msg = '<p><strong>Erreur avec la base de Données</strong></p>';
            include_once '../view/errorPage.php';
        }
        break;
}

